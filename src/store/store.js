import Vue from "vue";
import Vuex from "vuex";
import {PaginationPlugin} from 'vuex-pagination'
import firebase from "firebase/app";
import "firebase/auth"


Vue.use(Vuex);
Vue.use(PaginationPlugin)

export const store = new Vuex.Store({
    state:{
        dataList : [],
    },
    
    getters:{
        getAddedList(state){
            return state.dataList
        },
        getUser(){
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                  return user
                } else {
                  return user
                }
              });
        }
    },
    mutations :{
        clear_dataList (state){
            state.dataList = [];
        },
        addDataMutation(state,data){    //kişi ekleme
            Vue.http.post("https://telefon-rehber.firebaseio.com/products.json",data)
            .then( (response) => {
                state.dataList.push(data)
            })
            state.dataList.push(data)          
        },

        update2:(state,payload) => {        //güncelleme işlemi
            const {tc,ad,soyad,ev,is,cep} = payload
            let list = state.dataList;
            let ylist = list.filter(p => p.tc === tc)
            ylist[0].ad = ad
            ylist[0].soyad = soyad
            ylist[0].ev = ev
            ylist[0].is = is
            ylist[0].cep = cep

            let key = ylist[0].key;
            Vue.http.put("https://telefon-rehber.firebaseio.com/products/" + key + ".json", payload)
            .then(response => {
                state.dataList = ylist[0]
                list[key] = ylist[0]
                state.dataList = list
            }),response => {
            }
            state.dataList = ylist[0]
            list[key] = ylist[0]
            state.dataList = list
        },

        delete(state,del){          // silme işlemi ...
            let list = state.dataList;
            let ylist = list.filter(d => d.key === del)
            let yyy = ylist[0]
            let a2 = list.indexOf(yyy)
            list.splice(a2,1)
            let key2 = ylist[0].key;

            Vue.http.delete("https://telefon-rehber.firebaseio.com/products/" + key2 + ".json")
            .then(response => {
                  }),response => {
                    }
        },
    },
    actions:{ //async functions
        addDataAction(vuexContext,data){
            vuexContext.commit("addDataMutation",data)
            
        },
        initApp(){ //verilerimiz tabloya aktarmak
            Vue.http.get("https://telefon-rehber.firebaseio.com/products.json")
            .then(response => {
                this.state.dataList=[]
                let data = response.body;
                for(let key in data){
                    data[key].key = key;
                    this.state.dataList.push(data[key])
                }
            })
        }   
    }
})

export  default store